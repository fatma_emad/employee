package com.example.dms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dms.entity.Employee;
import com.example.dms.framework.AbstractMapper;
import com.example.dms.framework.AbstractRepository;
import com.example.dms.framework.AbstractServiceImpl;
import com.example.dms.mapper.EmployeeMapper;
import com.example.dms.repo.EmployeeRepository;
import com.example.dms.request.EmployeeRequest;
import com.example.dms.response.EmployeeResponse;

import lombok.Getter;

@Service
public class EmployeeService extends AbstractServiceImpl<EmployeeRequest, EmployeeResponse, Employee, Long> {

	private EmployeeRepository employeeRepository;

	@Autowired
	@Getter
	private EmployeeMapper employeeMapper;

	@Override
	protected AbstractMapper<Employee, EmployeeRequest, EmployeeResponse> getMapper() {
		return employeeMapper;
	}

	@Override
	protected AbstractRepository<Employee, Long> getRepository() {
		return employeeRepository;
	}

}
