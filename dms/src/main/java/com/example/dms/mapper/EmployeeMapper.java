package com.example.dms.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.dms.entity.Employee;
import com.example.dms.framework.AbstractMapper;
import com.example.dms.request.EmployeeRequest;
import com.example.dms.response.EmployeeResponse;

@Component
public class EmployeeMapper implements AbstractMapper<Employee, EmployeeRequest, EmployeeResponse> {

	@Override
	public Employee toEntity(EmployeeRequest request) {
		Employee employee = new Employee();
		employee.setBirthCity(null);
		employee.setBirthDate(null);
		employee.setContractType(null);
		employee.setDepartment(null);
		employee.setDirectManager(employee);
		employee.setEmployeeCode(null);
		employee.setEmployeeCode(null);
		employee.setEmployeeName(null);
		employee.setJobTitle(null);
		employee.setStatus(null);
		return employee;
	}

	@Override
	public void toEntity(EmployeeRequest request, Employee entity) {

	}

	@Override
	public EmployeeResponse toResponse(Employee entity) {

		EmployeeResponse employeeResponse = fillEmployeeResponse(entity);
		if (entity.getDirectManager() != null) {
			employeeResponse.setDirectManager(fillEmployeeResponse(entity.getDirectManager()));
		}
		return employeeResponse;
	}

	private EmployeeResponse fillEmployeeResponse(Employee entity) {
		EmployeeResponse employeeResponse = new EmployeeResponse();
		employeeResponse.setId(entity.getId());
		employeeResponse.setBirthCity(entity.getBirthCity());
		employeeResponse.setBirthDate(entity.getBirthDate());
		employeeResponse.setContractType(entity.getContractType());
		employeeResponse.setDepartment(entity.getDepartment());
		employeeResponse.setEmployeeCode(entity.getEmployeeCode());
		employeeResponse.setEmployeeName(entity.getEmployeeName());
		employeeResponse.setJobTitle(entity.getJobTitle());
		employeeResponse.setStatus(entity.getStatus());
		return employeeResponse;

	}

}
