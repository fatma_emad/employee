package com.example.dms.repo;

import org.springframework.stereotype.Repository;

import com.example.dms.entity.Employee;
import com.example.dms.framework.AbstractRepository;

@Repository
public interface EmployeeRepository extends AbstractRepository<Employee, Long> {

}