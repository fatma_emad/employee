package com.example.dms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.dms.constant.Department;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Table(name = "employee")
public class Employee {

	@Column(name = "employee_name", length = 50)
	private String employeeName;

	@Column(name = "employee_code", length = 50)
	private String employeeCode;

	@Column(name = "birth_date")
	private Date birthDate;

	@Column(name = "employee_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "department", length = 45)
	@Enumerated(EnumType.STRING)
	private Department department;

	@Column(name = "contract_type", length = 45)
	private String contractType;

	private Boolean status;
	private String jobTitle;
	private String birthCity;	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_employee_id")
	private Employee directManager;

}
