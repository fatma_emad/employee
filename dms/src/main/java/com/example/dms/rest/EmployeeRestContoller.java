package com.example.dms.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dms.criteria.EmployeeCriteria;
import com.example.dms.entity.Employee;
import com.example.dms.framework.AbstractRestControllerImpl;
import com.example.dms.framework.AbstractService;
import com.example.dms.request.EmployeeRequest;
import com.example.dms.response.EmployeeResponse;
import com.example.dms.service.EmployeeService;

import lombok.Getter;

@RestController
@RequestMapping("/employee")
public class EmployeeRestContoller
		extends AbstractRestControllerImpl<EmployeeRequest, EmployeeResponse, EmployeeCriteria, Employee, Long> {
	@Autowired
	@Getter
	private EmployeeService employeeService;

	@Override
	protected AbstractService<EmployeeRequest, EmployeeResponse, Employee, Long> getService() {
		return employeeService;
	}


}
