package com.example.dms.parameters;

import java.util.Date;

import com.example.dms.constant.Department;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeSearchParameters {
	private Long id;
	private String employeeName;
	private String employeeCode;
	private Date birthDate;
	private Department department;
	private String contractType;
	private Boolean status;
	private String jobTitle;
	private String birthCity;
	private Boolean parentIdIsNull;
	private Long directManagerId;
}
