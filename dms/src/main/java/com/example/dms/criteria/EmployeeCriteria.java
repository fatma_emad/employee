package com.example.dms.criteria;

import com.example.dms.entity.Employee;
import com.example.dms.parameters.EmployeeSearchParameters;
import com.example.dms.specification.SpecificationBuilder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class EmployeeCriteria extends SpecificationBuilder<Employee> {
	private static final long serialVersionUID = 1L;
	private EmployeeSearchParameters parameters;

	@Override
	protected void fillExpressions() {
		of("id").is(parameters.getId());

		of("birthCity").is(parameters.getBirthCity());

		of("contractType").is(parameters.getContractType());

		of("status").is(parameters.getStatus());

		of("jobTitle").is(parameters.getJobTitle());

		of("department").is(parameters.getDepartment());

		of("birthDate").is(parameters.getBirthDate());

		of("employeeCode").is(parameters.getEmployeeCode());

		of("employeeName").is(parameters.getEmployeeName());
		if (Boolean.TRUE.equals(parameters.getParentIdIsNull())) {
			of("directManager.id").isNull();
		}

	}

}
