package com.example.dms.framework;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.domain.Specification;

@Transactional
public abstract class AbstractServiceImpl<REQ, RES, E, ID> implements AbstractService<REQ, RES, E, ID> {

	protected abstract AbstractMapper<E, REQ, RES> getMapper();

	protected abstract AbstractRepository<E, ID> getRepository();

	@Override
	public List<E> searchEntity(Specification<E> criteria) {

		var entitys = getRepository().findAll(criteria);
		return entitys;
	}

	@Override
	public List<RES> search(Specification<E> criteria) {

		var entitys = searchEntity(criteria);
		List<RES> responses = getMapper().toResponses(entitys);
		return responses;
	}

	@Override
	public void deleteById(ID id) {
		getRepository().deleteById(id);
	}

	@Override
	public RES update(ID id, REQ request) {

		E entity = updateInTransaction(id, request);
		return getMapper().toResponse(entity);
	}

	public E updateInTransaction(ID id, REQ request) {

		E entity = getRepository().findById(id).get();
		getMapper().toEntity(request, entity);
		entity = getRepository().save(entity);

		return entity;
	}

	public E findByIdEntity(ID id) {
		E entity = getRepository().findById(id).get();
		return entity;
	}

	@Override
	public RES findById(ID id) {
		E entity = findByIdEntity(id);
		RES response = getMapper().toResponse(entity);
		return response;
	}

	@Override
	public List<RES> findAll() {
		List<E> list = (List<E>) getRepository().findAll();
		List<RES> responses = getMapper().toResponses(list);

		return responses;
	}
	@Override
	public RES create(REQ request) {

		E entity = createInTransaction(request);

		var response = getMapper().toResponse(entity);
	
		return response;
	}
	public E createInTransaction(REQ request) {

		E entity = getMapper().toEntity(request);

		entity = getRepository().save(entity);
	
		return entity;
	}

}
