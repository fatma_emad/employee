package com.example.dms.framework;

import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public interface AbstractMapper <E, REQ, RES> {

	E toEntity(REQ request);

	abstract void toEntity(REQ request, E entity);

	abstract RES toResponse(E entity); 

	default Date toDate(Long millis) {

		if (millis == null) {
			return null;
		}
		return new Date(millis);

	}

	default Long toMillis(Date date) {
		if (date == null) {
			return null;
		}
		return date.getTime();
	}

	default Date now() {
		return Date.from(Instant.now());
	}

	default List<RES> toResponses(List<E> list) {
		List<RES> dtos = new LinkedList<>();
		if (list == null)
			return null;
		for (E entity : list) {
			RES dto = toResponse(entity);
			dtos.add(dto);
		}

		return dtos;
	}

}

