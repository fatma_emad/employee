package com.example.dms.framework;

import java.util.List;

import com.example.dms.specification.SpecificationBuilder;

public interface AbstractController <REQ, RES, C extends SpecificationBuilder<E>, E, ID> {
	public RES create(REQ dto);

	public RES update(ID id, REQ dto);

	public void delete(ID id);

	public RES findById(ID id);

	public List<RES> findAll();

	public List<RES> search(C criteria);


}
