package com.example.dms.framework;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.dms.specification.SpecificationBuilder;

public abstract class AbstractRestControllerImpl<REQ, RES, C extends SpecificationBuilder<E>, E, ID>
		implements AbstractController<REQ, RES, C, E, ID> {

	protected abstract AbstractService<REQ, RES, E, ID> getService();

	@Override
	@PostMapping
	public RES create(REQ request) {
		return getService().create(request);
	}

	@Override
	@PutMapping("/{id}")
	public RES update(@PathVariable("id") ID id,  @RequestBody REQ request) {
		return getService().update(id, request);
	}

	@Override
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") ID id) {
		getService().deleteById(id);

	}

	@GetMapping("/{id}")
	public RES findById(@PathVariable("id") ID id) {
		return getService().findById(id);
	}

	@Override
	@GetMapping
	public List<RES> findAll() {
		return getService().findAll();

	}

	@Override
	@GetMapping("/search")
	public List<RES> search( @RequestBody C criteria) {
		List<RES> res = getService().search(criteria);
		return res;
	}

}
