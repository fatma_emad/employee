package com.example.dms.framework;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

public interface AbstractService<REQ, RES, E, ID> {

	public RES findById(ID id);

	public RES create(REQ dto);

	public RES update(ID id, REQ dto);

	public void deleteById(ID id);

	public List<RES> findAll();

	public List<RES> search(Specification<E> criteria);
	public List<E> searchEntity(Specification<E> criteria);
}
