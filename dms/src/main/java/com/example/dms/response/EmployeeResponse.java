package com.example.dms.response;

import java.util.Date;

import com.example.dms.constant.Department;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public class EmployeeResponse {
	private Long id;
	private String employeeName;
	private String employeeCode;
	private Date birthDate;
	private Department department;
	private String contractType;
	private Boolean status;
	private String jobTitle;
	private String birthCity;

	private EmployeeResponse directManager;
}
